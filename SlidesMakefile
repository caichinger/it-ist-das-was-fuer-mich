# Slides
SOURCE_DIR = ./source/course/
SLIDES_SOURCE = $(wildcard $(SOURCE_DIR)/day_*/slides.md)
SLIDES_PDF = $(patsubst %.md, %.pdf, $(SLIDES_SOURCE))
SLIDES_HTML = $(patsubst %.md, %.html, $(SLIDES_SOURCE))

MAKEFLAGS := --jobs=$(shell nproc)  # TBD: does this work on Windows?

# print help
.PHONY : help
help : SlidesMakefile
	@sed -n 's/^##//p' $<

## all         : Compile all slides.md to create PDF/HTML.
.PHONY: all
all : $(SLIDES_HTML) $(SLIDES_PDF)

## pdfs        : Create PDFs.
.PHONY : pdfs
pdfs : $(SLIDES_PDF)

$(SOURCE_DIR)/day_%/slides.pdf : $(SOURCE_DIR)/day_%/slides.md
	npx @marp-team/marp-cli@latest --allow-local-files $< -o $@

## htmls       : Create HTMLs.
.PHONY : htmls
htmls : $(SLIDES_HTML)

$(SOURCE_DIR)/day_%/slides.html : $(SOURCE_DIR)/day_%/slides.md
	npx @marp-team/marp-cli@latest --allow-local-files $< -o $@

## clean       : Remove auto-generated files.
.PHONY : clean
clean :
	rm -f $(SLIDES_HTML)
	rm -f $(SLIDES_PDF)

## variables   : Print variables (for debugging).
.PHONY : variables
variables:
	@echo SLIDES_SOURCE: $(SLIDES_SOURCE)
	@echo SLIDES_PDF: $(SLIDES_PDF)
	@echo SLIDES_HTML: $(SLIDES_HTML)
