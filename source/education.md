# Bildungswege

*Entweder direkt hier die Website aktualisieren oder im [Google Doc](https://docs.google.com/document/d/1PykTpxyFCgST-wZjfGTPiP55jg8Egk7RLA1YWifUtgI/edit?usp=sharing).*

## Lehre

### Beispiele

*Welche Lehren gibt es im IT Bereich?*

* Applikationsentwicklung – Coding,
* Informationstechnologie – Schwerpunkt Systemtechnik
* Informationstechnologie –Schwerpunkt Betriebstechnik
* ELEKTROTECHNIK – ANLAGEN- UND BETRIEBS­TECHNIK
* EDV-Kaufmann / EDV-Kauffrau
* ELEKTRONIK – INFORMATIONS- UND TELE­KOMMUNIKATIONS­TECHNIK

Links:
* https://www.oeamtc.at/karriere-portal/ (gibt auch Angebote für Lehrlinge)
* https://www.nasicher.at/de/technisch
* https://www.wienerlinien.at/lehrlingsausbildung
* Und viele mehr!


### Voraussetzungen

*Welche Voraussetzungen muss man mitbringen, um eine Lehre zu absolvieren?*

* **Pflichtschulabschluss**
* Du hast Spaß am Umgang mit PCs, Software und Apps.
* Du bist kommunikativ, arbeitest gerne im Team und liebst neue Herausforderungen.
* Du arbeitest gerne selbstständig und eigenverantwortlich.
* Du arbeitest gerne mit Menschen und kannst auf Kund:innen eingehen.
* Du bist bereit, dich im Laufe deiner Karriere weiterzubilden und bezüglich Hard- und Software auf dem neusten Stand zu bleiben.


### Dauer

*Wie lange dauert eine Lehre?*

3-4 Jahre (mit Matura evtl. kürzer)

### Finanzen

*Welche finanzielle Unterstützung/Entschädigung bekommen man?*

Lehrlingsentschädigung:
1. Lehrjahr: 610,- €
1. Lehrjahr: 770,- €
1. Lehrjahr: 950,- €
1. Lehrjahr: 1300,- €


## Kurse, Bootcamps, Weiterbildungen, ...

### Beispiele

*Welche Angebote gibt es?*

* [Software Entwickler/innen (Förderungsfonds Waff )](https://www.waff.at/jobs-ausbildung/jobs-mit-ausbildung/it-jobs-informationstechnologie/junior-software-developer/)
* [UX design](https://everyonecodes.io/programs/ux-design)
* [WEB DEVELOPMENT](https://upleveled.io/web-development-bootcamp)
* [Javascript](https://www.smartninja.at/javascript)
* [Haking und Internet security](https://www.smartninja.at/hacking-und-internet-security)
* Und viele mehr!

### Voraussetzungen

*Welche Voraussetzungen muss man mitbringen, um diese Kurse zu absolvieren?*

* Matura, Berufsreifeprüfung, abgeschlossene Lehre.
* Beim AMS Wien arbeitslos oder arbeitsuchend gemeldet.
* Mindestalter: 20 Jahre.
* Hauptwohnsitz Wien.
* Sehr gute EDV-Kenntnisse.
* Gute deutschkenntnisse (Niveau c1).
* Gute Englischkenntnisse (Niveau B2).

### Dauer

*Wie lange dauern diese Angebote?*

12 Monate (je nach Kurs)

### Finanzen

*Wie viel kosten solche Kurse?*

Von Kurs zu Kurs verschieden. Förderung durch AMS möglich.


## Schule und Studium

### Beispiele

*Welche Studienrichtungen gibt es mit IT Bezug?*

## TU Wien

![logo](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/TU_Logo.svg/200px-TU_Logo.svg.png)

* Medieninformatik und Visual Computing
* Medizinische Informatik
* Software & Information
* Technische Informatik
* Wirtschaftsinformatik
* Medieninformatik und Visual Computing
* Medizinische Informatik
* Software & Information Engineering
* Technische Informatik
* Wirtschaftsinformatik

## FH Campus Wien
![logo](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/FHCampusWien_inkl-gesch%C3%BCtzterBereich.svg/169px-FHCampusWien_inkl-gesch%C3%BCtzterBereich.svg.png)

* Angewandte Elektronik
* High Tech Manufacturing
* Computer Science and Digital Communications
* Clinical Engineering

### Voraussetzungen

*Welche Voraussetzungen muss man mitbringen, um Studium zu absolvieren*

* Allgemeine Hochschulreife:
    - Reifezeugnis einer Allgemeinbildenden oder Berufsbildenden Höheren Schule
    - Berufsreifeprüfung
    - Gleichwertiges ausländisches Zeugnis (=gleichwertig ist es, wenn es völkerrechtlich vereinbart ist oder nostrifiziert wurde. Die Studiengangsleitung kann das Zeugnis auch im Einzelfall anerkennen.)
* Studienberechtigungsprüfung (Elektrotechnik oder Informatik)
* Studienbeihilfe zur Studienberechtigungsprüfung/Zusatzprüfung
* Deutschkenntnisse B2
* Einschlägige berufliche Qualifikation mit Zusatzprüfungen


### Dauer

*Wie lange dauert ein Studium?*
* Die Studiengänge an der TU Wien und der FH Campus Wien dauern im Durchschnitt 6 Semester.

### Finanzen

*Welche finanzielle Unterstützung/Entschädigung bekommen man?*

* Studienbeihilfe
   - Zwischen 5 € -  475 € im Monat
   - Ausnahme 679 € für Studierende (Entfernung am Studienort, Eltern verstorben,  Selbsterhalter*innen-Stipendium beziehende)
   - bei Bezug der Studienbeihilfe erhalten die Studierenden den Studienbeitrag von 376,36 Euro zurück erstattet.
* Leistungsstipendium der FH Campus Wien
   - Voraussetzung ist ein Notendurchschnitt von ≤ 2,00 (auf zwei Kommastellen gerundet). Für die Berechnung werden die Studienleistungen
   Studienjahres herangezogen. Die Höhe des Stipendiums beträgt rund 1.000 Euro, ist jedoch abhängig von der Anzahl der Studierenden,
   die ein Stipendium beantragen
* Zusätzliche Beihilfen:
* Wohnbeihilfe (MA 50)
* Familienbeihilfe
* WAFF - Wiener Arbeitnehmer*innen Förderungsfonds
* WAFF - Wiener Arbeitnehmer*innen Förderungsfonds – Stiftung Jugend & Zukunftsberufe
  -für ausgewählte Bachelorstudiengänge in den Bereichen Gesundheits- und Krankenpflege, Soziales und Technik,..
* NÖ Bildungsförderung
* AMS
* Hier kann man nachsehen, für welche Förderungen man die Anforderungen erfüllt:
 <https://erwachsenenbildung.at/bildungsinfo/kursfoerderung/>









