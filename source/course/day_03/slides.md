---
marp: true
theme: default
header: '![width:200px](../../_static/ec_logo_cropped.png)'
footer: 'IT, ist das was für mich? Slides von Claus Aichinger & Olga Drewitz (it-orientation@everyonecodes.io)'
paginate: true
---

# IT, ist das was für mich? 👩‍💻

## Tag 3 🎈

**Aufgaben & Anleitungen & Algorithmen**

---

## Intro 🌅

### Was passiert heute? 🗒️

- Aufgaben beschreiben
- Algorithmen ableiten

- Eine Aufgabe für Olga's Roboter ausarbeiten

---

### Was war gestern? 🕥

**Woraus besteht ein Computer?**

Eine kleine Wiederholung:

- Welche Komponenten kennst du?
- Was machen diese?
- Optional: Wie werden sie beschrieben?

---

## Dinge, Aufgaben und Lösungen beschreiben 🤔

Ein paar Gedanken:

- Was ist das Ding?
- Woraus besteht es, wie ist es entstanden?
- Wie kann ich eine Anleitung schreiben, damit es
  eine andere Person (mit vergleichbaren Fähigkeiten)
  nachbauen kann?

---

### Übung: Wir probieren das gemeinsam

*Was ist dieses Ding?*

*Wie macht man es?*

---

# Pause ☕

*Gleich geht's weiter.*

---

### Übung: Ihr seid dran!

Wir bilden 3 Gruppen, jede Gruppe bekommt 2 Dinge, jede Gruppe ist in einem eigenen Raum.

In der Gruppe:
1. Besprechen: Was ist das Ding? Wie baut man es?
1. Ausarbeiten: Erstellt eine Anleitung zum Nachbau (für eine andere Gruppe)

Danach: eine andere Gruppe versucht gemäß eurer Anleitung das Ding nachzubauen.

Danach: Nachbesprechung

---

# Pause ☕

*Gleich geht's weiter.*

---

## Anleitung ➡️ Programm

- Anleitung: Beschreibung von Menschen für Menschen
- Programm: Anleitung von Menschen für Computer (und Menschen)

**Wichtig, dass wir uns klar ausdrücken!**

- Was wollen wir?
- Wie soll es passieren?

---

## Anleitung ➡️ Programm

**Programmieren heißt:**
1. Nachdenken: Was will ich? ➡️ Idee 💡
1. Beschreiben: Was will ich genau? ➡️ Beschreibung/Anleitung 📝
1. Übersetzen: Das will ich! ➡️ Programm 🤖

---

### Übung: Eine Aufgabe & Anleitung für Olga

Wir:
1. Nachdenken: Was soll der Roboter machen? ➡️ Idee 💡
1. Beschreiben: Was wollen wir genau? ➡️ Beschreibung/Anleitung 📝

Olga:
3. Übersetzen: Das wollt ihr?! ➡️ Programm 🤖

*Wird Olga verstehen, was wir wollen?*
*Wird der Roboter verstehen, was Olga will?*

---

### Übung: Eine Aufgabe & Anleitung für Olga

Ein Roboter kann…
- wie ein Auto in alle Richtungen fahren (Dauer, Strecke)
- in verschiedenen Geschwindigkeiten fahren (1-100)
- folgendes sagen: Farben, Good Job, Bravo, Okey-dokey

Was soll der Roboter machen?

---

## Pause ☕

*Gleich geht's weiter.*

---

## Outro 🌆

*Tagesabschluss*

---

### Was war heute?

- Dinge beschreiben.
- Aufgaben beschreiben und Anleitungen erstellen.
- Anforderungen übersetzen.

---

### Was haben wir heute gelernt? 📝

**Welche Begriffe & Zusammenhänge waren euch wichtig?**

---

### Was kommt morgen?

- Fortsetzung von heute
- Erste Schritte mit den Robotern

---

### Retrospektive

- Was hast du gelernt?
- Was hat dich überrascht?
- Was nimmst du mit für morgen?

---

# Wir freuen uns auf morgen! 😃
