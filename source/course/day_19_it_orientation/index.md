# Tag 19: Berufe und Ausbildungen

**Welche Wege führen in die IT?** 👩‍💻

Wir haben Krisztina, die gerade in Ausbildung ist, Sophie, die als Junior Developer Spiele
entwickelt und Dagmar, die Projekte leite und Teams organisiert.

Wie könnte dein Weg in die IT aussehen?
Genau ist heute unser Schwerpunkt!

{download}`🎬 Slides <./slides.pdf>`


