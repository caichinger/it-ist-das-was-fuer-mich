---
marp: true
theme: default
header: '![width:200px](../../_static/ec_logo_cropped.png)'
footer: 'IT, ist das was für mich? Slides von Alexandra Pediacova (it-orientation@everyonecodes.io)'
paginate: true
---

# IT, ist das was für mich? 👩‍💻

## Tag 19

> Welche Wege führen in die IT?

*Daniela ist zwar wieder gesund aber ihr Sohn ist leider krank*

---

# Intro 🌅

## Was passiert heute? 🗒️

1. Welche Arten von Ausbildungen gibt es?
2. Welche Ausbildungen sind konkret für dich interessant?
3. Berufsbilder oder Projekte

---

# Welche Arten von Ausbildungen gibt es?

Wir erarbeiten gemeinsam eine Übersicht zu verschiedenen
Bildungswegen.

---

# Welche Ausbildungen sind konkret für dich interessant?

Recherchiere Ausbildungen und aktualisiere dein Profil.

---

# Berufsbilder oder Projekte

...

---

# Outro 🌆

## Was kommt morgen?

- Projekt Kick Off 🚀

---

# Ich freue mich auf morgen! 😃
