# Tag 18: Gäste aus der IT

**Was machen Projektleiterinnen, Scrum Master und Spielentwicklerinnen?** 👩‍💻

Heute kommen uns zwei Gäste besuchen:
* 13:00: Dagmar Labes, Projektleiterin und Scrum Master, bei [Sportradar](https://www.sportradar.com/) und
* 16:00: Sophie Amelien, Game Dev, bei [mi'pu'mi](https://www.mipumi.com/).

Gestern haben wir uns einige Fragen ({ref}`hier <exercise-questions-for-dagmar>`
und {ref}`hier <exercise-questions-for-sophie>`) für sie überlegt, heute bekommen wir Antworten.

In der Übergangszeit werden wir an eurer Kursseite weiter arbeiten und uns
erste Gedanken zur Projektwoche machen.

{download}`🎬 Slides <./slides.pdf>`

(sophie_gamedev_doc_target)=
Sophie war so freundlich uns folgende Dokumente zur Verfügung zu stellen:
* {download}`Game Dev: Ausbildungen in Österreich <./AusbildungenInOesterreich.pdf>`
* {download}`Game Dev: Jobs in der Spieleindustrie <./JobsInDerSpieleIndustrie.pdf>`
