---
marp: true
theme: default
header: '![width:200px](../../_static/ec_logo_cropped.png)'
footer: 'IT, ist das was für mich? Slides von Claus Aichinger (it-orientation@everyonecodes.io)'
paginate: true
---

# IT, ist das was für mich? 👩‍💻

## Tag 18

> Was machen Projektleiterinnen, Scrum Master und Game Devs?

---

# Intro 🌅

## Was passiert heute? 🗒️

- Gastvorträge und Nachbesprechung
- Ergänzen der Kursseite
- Gedanken zur Projektwoche

---

# Weiter auf unserer Website

**https://caichinger.gitlab.io/it-ist-das-was-fuer-mich/**

---

# Outro 🌆

## Was kommt morgen?

- TODO

---

# Ich freue mich auf morgen! 😃
