# Tag 4: Der Ev3

**Wir programmieren!** 🤓 📝

Der heutige Tag dreht sich um ausprobieren und anfassen.
Du hast heute die Möglichkeit auszuprobieren und testen. Alles was Spaß macht und dich interessiert.

Am Ende des Tages trage deinen Fortschritt bitte in die folgende Liste ein:


[Dokumentation Fortschritt](https://docs.google.com/spreadsheets/d/1jutpvxVylv6WQlVsX-_hYSgHtxaCSIS9V22MC61zEmc/edit?usp=sharing)

