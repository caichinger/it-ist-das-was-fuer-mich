---
marp: true
theme: default
header: '![width:200px](../../_static/ec_logo_cropped.png)'
footer: 'IT, ist das was für mich? Slides von Claus Aichinger (it-orientation@everyonecodes.io)'
paginate: true
---

# IT, ist das was für mich? 👩‍💻

## Tag 16

✂️ + 🪨 + 📑 = 😃

Abschluss Micro Bit

Recherche zu Berufen und Ausbildungen

---

# Intro 🌅

## Was passiert heute? 🗒️

- Recherche zu Berufsausbildungen
- Schere, Stein, Papier fertig machen

---

# Wie war Soft Skills? 🤔

- Was habt ihr gemacht?
- Wie ist es euch gegangen?

---

## Was haben wir heute gelernt? 📝

## Begriffe & Zusammenhänge

*Was war euch wichtig?*

---

## Outro 🌆

### Was war heute?

- Berufe
- Ausbildungen
- Wie sich ein größeres Programm Stück für Stück zusammen setzt

---

### Was kommt morgen

- Weiter bei Berufen und Ausbildungsprogrammen
- Projekte

---

## Retrospektive

- Was hast du gelernt?
- Was hat dich überrascht?
- Was nimmst du mit für morgen?

---

# Wir freuen uns auf morgen! 😃
