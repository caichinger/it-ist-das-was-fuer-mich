# Tag 14: Web Intro

**Wie funktionieren Webseiten?** 🌐 🤔

Zusätzlich zum Microbit wollen wir heute auch eine neue Frage aufgreifen:

> Was ist eine Website?

{download}`🎬 Slides <./slides.pdf>`


Wir machen heute folgende Übung(en):
- {ref}`exercise-basic-radio`
- {ref}`Schere-Stein-Papier (Teil 2) <exercise-rock-paper-scissor-part-2>`

Für morgen bitte [Visual Studio Code](https://code.visualstudio.com/)
herunterladen und installieren. Wir werden uns anschauen, wie man
Webseiten erstellen kann.
