---
marp: true
theme: default
header: '![width:200px](../../_static/ec_logo_cropped.png)'
footer: 'IT, ist das was für mich? Slides von Olga Drewitz und Claus Aichinger (it-orientation@everyonecodes.io)'
paginate: true
---

# IT, ist das was für mich? 👩‍💻

## Tag 21 - Tag 24: Projektarbeit 🚀

> Ready, steady, gooo!

*Siehe Slides von Tag 20.*

---

## Retrospektive

- Konntest du deine Ziele für heute umsetzen?

---

# Wir freuen uns auf die nächste Einheit! 😃
