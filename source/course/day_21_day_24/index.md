# Tag 21 - Tag 24: Projektarbeit

**Projektarbeit!** 🚀

{download}`🎬 Slides <./slides.pdf>`


## Tag 21

Schwerpunkte:
* Projektbeschreibung, siehe {ref}`tag20_project_kickoff_target`
* Feedback von letzter Woche, falls noch nicht ausgefüllt, siehe {ref}`tag20_project_kickoff_target`


## Tag 24 (heute)

Ziele:
* Wenn du noch Fragen hast, wie es nach dem Kurs weiter geht, melde dich bei uns
* Projekte zu einem Abschluss bringen (es ist voll okay, wenn ihr nicht ganz fertig seid)
* Präsentationen erstellen
  * Worum geht es in eurem Projekt? Stelle dir vor du beschreibst deine Idee einer Person
    die noch gar nichts von dir und diesem Kurs gehört hat.
  * Was war eurer Ziel?
  * Wie habt ihr es umgesetzt?
  * Was habt ihr dabei gelernt?
* Sofern ihr Code geschrieben habt
  * Web: ZIP Datei erstellen und per Mail an it-orienation@everyonecodes.io schicken
  * MakeCode: Screenshot eurer Projekte und eine Kopie
    des Quellcodes (JavaScript oder Python) in euer Projektdokument einfügen

Sophie war so nett uns aus ihrer Arbeit zwei Dokumente zu schicken, in denen es um
Ausbildungen und Jobs im Game Dev Bereich geht. Du findest sie {ref}`hier <sophie_gamedev_doc_target>`.


## Call mit Claus

Claus, am 21.02.2022: Mein letzter PCR-Test ist leider positiv ausgefallen und ich
bin daher die nächsten Tage auf jeden Fall in Quarantäne. Ich hoffe, dass ich aber
zumindest online dabei sein und euch im Kurs begleiten kann.
Zu diesem Zweck habe ich eine [Videokonferenz](https://meet.google.com/juo-ehyn-egk) erstellt,
über die wir uns austauschen können. Das sollte ohne weitere Installationen direkt im
Browser funktionieren.
