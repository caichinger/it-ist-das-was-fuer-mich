# Tag 10: Peer Learning

Heute steht Peer Learning am Programm.


```{include} ../_peer_learning.md
```


Dokumentiere deinen heutigen Fortschritt in [diesem Dokument](https://docs.google.com/document/d/1CxwnVpp9fDHrhxjvI-Q5rkCeh0kwwojFHUyKRAXHRf4/edit?usp=sharing).


```{exercise} Wie war die Woche?
:label: exercise-feedback-week-2

Beantworte dazu folgende Fragen über dich:
- Was hat dir diese Woche besonders gut gefallen? Wieso hat dir das besonders gut gefallen?
- Was hat dir diese Woche gar nicht gefallen? Wieso hat dir das nicht gefallen?

```


```{exercise} Das Spielfeld - Eine Aufgabe lösen
:label: exercise-week2-solve-a-exercise

Gestern haben wir gemeinsam und du alleine versucht eine Aufgabe auf dem Spielfeld für den EV3 zu lösen. Du hast heute die Möglichkeit dies selber auszuprobieren. Deine ToDo's sind:
- Beschreibe die Aufgabe, die du machen möchtest
- Überlege dir, was der Roboter braucht um die Aufgabe zu lösen
- Schreibe einen Algorithmus/Lösungsweg für den Roboter
- Programmiere die Aufgabe
- Schau nach ob der Algorithmus gepasst hat? Was musstest du verändern?

Beantworte die Aufgane in [diesem Dokument](https://docs.google.com/document/d/1CxwnVpp9fDHrhxjvI-Q5rkCeh0kwwojFHUyKRAXHRf4/edit?usp=sharing).
```

Am Ende des Tages sollst du bitte die folgenden Fragen beantowrten.

```{exercise} Das Spielfeld - Reflexion
:label: exercise-week2-reflexion-exercise

Diese Aufgabe ist für den Abschluss des Tages, diskutiere sind mit einer weiteren Person. Ihr müsste dazu nichts notieren.
Beantworte die folgenden Fragen:
- Wie hat es dir heute gefallen die Aufgabe auf dem Spielfeld zu lösen?
- Wie hast du dich dabei gefühlt die Aufgabe zu lösen?
- Könntest du dir vorstellen das jeden Tag zu machen?
```

Diese Aufgabe ist für dich alleine:

```{exercise} Was gefällt dir an der IT?
:label: exercise-motivation-it-2

Du hattest in den letzten tagen zeit etwas zu programmieren. Du hast auch schon einige Konzepte der Programmierung kennengelernt. Außerdem konntest du den Roboter umbauen und seine Motoren/Sensoren verwenden.

Stelle dir vor, du erzählst einer Freundin, dass du den Berufsorientierungskurs "IT, ist das
was für mich?" besuchst.

Wie würdest du diese Frage "IT, ist das was für mich?" im Moment beantworten und warum?

Denke nach und schreibe einen kleinen Aufsatz in [diesem Dokument](https://docs.google.com/document/d/1CxwnVpp9fDHrhxjvI-Q5rkCeh0kwwojFHUyKRAXHRf4/edit?usp=sharing).
```


