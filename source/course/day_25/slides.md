---
marp: true
theme: default
header: '![width:200px](../../_static/ec_logo_cropped.png)'
footer: 'IT, ist das was für mich? Slides von Olga Drewitz und Claus Aichinger (it-orientation@everyonecodes.io)'
paginate: true
---

# IT, ist das was für mich? 👩‍💻

## Tag 25

Abschluss 🎊

---

# Intro 🌅

## Was passiert heute? 🗒️

* Frühstück
* Projektpräsentationen
* Zertifikate
* Feedback
* Nächste Schritte

---

# Frühstück

Mahlzeit! 🥐 ☕

---

# Projektpräsentationen

* Was habt ihr gemacht?
* Wie ist es euch gegangen?
* Was nehmt ihr mit?

Fragen stellen ist erwünscht.

---

# Zertifikate 📄

Schön, dass ihr dabei wart!

Wir hoffen, ihr konntet einiges mitnehmen!

---

# Feedback

Bitte füllt das Feedback Formular auf unserer Kursseite aus.

---

# Nächste Schritte

## Wenn ihr noch nicht wisst, wie es weiter gehen soll...

▶️ Es gibt tolle Beratungsangebote!

* Mädchen-Berufs-Zentrum (MBZ) bei sprungbrett
* Frauenberufszentrum (FBZ)
* Lobby16
* A|B|O Jugend

---

## Wenn ihr in die IT gehen wollt...

▶️ Das AMS bekommt unsere Empfehlung für euch!

* Wir werden auch mit der Kursverantwortlichen beim AMS
  reden, damit ihr die notwendige Unterstützung bekommt.
* Karrierewege sind nicht immer gerade - das ist okay.
* Viel Erfolg bei den weiteren Ausbildungen!

---

## Wenn ihr etwas anderes tun werdet...

Auch da wünschen wir euch alles, alles Gute!

---

# Toll, dass ihr dabei wart! 😃
