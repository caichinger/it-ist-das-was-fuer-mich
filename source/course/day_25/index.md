---
substitutions:
  feedback_link: '[Feedback](https://docs.google.com/forms/d/e/1FAIpQLSfI_alCmHNLTvoC2-5NYwsLO9inyt5__8KDgjc3-OqMUGqNPQ/viewform?usp=sf_link)'
---
# Tag 25: Frühstück, Abschluss und Ausblick

**Auf zu neuen Ufern!** ☕ 🥐 🚀

Wir wollen die vergangenen 5 Wochen mit euren Präsentationen zum
Abschluss bringen und nächste Schritte besprechen.

{download}`🎬 Slides <./slides.pdf>`


**Wir freuen uns sehr, dass du dabei warst und wünschen dir für
den weiteren Berufsweg alles, alles Gute!!**


## Feedback

Die Umfrage ist etwas ausführlicher als sonst, weil es um
den Kurs als Ganzes geht.

% supply link to feedback form in through the preamble's feedback_link variable
{{ ask_for_feedback }}


## Webseite und Weiterlernen

Die Webseite wird noch ein paar Tage online bleiben.

Wenn es Seiten gibt, die ihr speichern wollt, ladet diese bitte
als PDF oder Markdown herunter (siehe Download-Button recht oben).

Ein paar Ressourcen zum eigenständigen Lernen haben wir
unter {ref}`Selber Lernen <learning_target>` gesammelt.

**Traut euch! 🌟**


## Claus verabschiedet sich 👋 ❤️

> Liebe Teilnehmerinnen!
>
> Es freut und ehrt mich, dass ihr euch in den vergangenen
> fünf Wochen auf unsere *Reise in die IT* eingelassen habt.
> Wir haben viel erlebt, viel gesehen und viel gelernt.
> Es war eine tolle Erfahrung mit euch zu arbeiten und zu sehen,
> welche Projekte ihr nach so kurzer Zeit auf die Beine gestellt habt!
> Ihr könnt zurecht Stolz auf euch sein!
>
> Ich möchte diejenigen von euch, die den Weg in die IT einschlagen wollen,
> sehr herzlich dazu ermutigen diesen Schritt zu wagen.
> Es wird sicher nicht immer einfach sein, oft auch fordernd
> aber auch unglaublich lohnend!
>
> Jeder einzelnen von euch wünsche ich alles, alles Gute!
>
> Ganz liebe Grüße aus der Quarantäne, <br>
> Claus
>
> PS: Es würde mich freuen in Zukunft von euch zu hören.

