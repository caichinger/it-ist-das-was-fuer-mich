# Tag 13: Programmieren mit dem Microbit II

**Ideen weiterentwickeln!** 🎊

Wir haben in den vergangenen Einheiten wichtige Konzepte
wie Variablen, Verzweigungen und Schleifen eingeführt
sowie nützliche Code Blöcke kennen gelernt.

Hier wollen wir auch heute wieder anknüpfen und uns insbesondere
darüber Gedanken machen, wie man verschiedene Komponenten eines
Programms zusammen führen kann.

{download}`🎬 Slides <./slides.pdf>`


Wir machen heute folgende Übung(en):
- {ref}`exercise-micro-bit-retrospective`
- {ref}`Schere-Stein-Papier (Teil 1) <exercise-rock-paper-scissor-part-1>`
- {ref}`exercise-basic-radio`
- {ref}`Schere-Stein-Papier (Teil 2) <exercise-rock-paper-scissor-part-2>`
