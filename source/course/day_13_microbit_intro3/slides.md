---
marp: true
theme: default
header: '![width:200px](../../_static/ec_logo_cropped.png)'
footer: 'IT, ist das was für mich? Slides von Claus Aichinger (it-orientation@everyonecodes.io)'
paginate: true
---

# IT, ist das was für mich? 👩‍💻

## Tag 13

> Wie entwickelt man ein Programm?

---

# Intro 🌅

## Was passiert heute? 🗒️

- Kleine Wiederholung
- Wie entwickelt man ein Programm?

---

# Wie entwickelt man ein Programm?

*Siehe Slides von gestern*

---

# Weiter auf unserer Website

**https://caichinger.gitlab.io/it-ist-das-was-fuer-mich/**

---

# Outro 🌆

## Was haben wir heute gemacht? 📝

- Wichtige Begriffe
- Wichtige Zusammenhänge

---

## Was kommt morgen?

- Weiter beim Microbit und Vorbereitung für Peer Learning
- Vorbereitung für unseren Gast Krisztina
- Was ist eine Website?

---

## Retrospektive

- Was hast du gelernt?
- Was hat dich überrascht?
- Was nimmst du mit für morgen?

---

# Ich freue mich auf morgen! 😃
