# Tag 5: Peer Learning

Gratulation zur ersten Kurswoche! 🎊

Heute steht Peer Learning am Programm.


```{include} ../_peer_learning.md
```

Wir haben uns einige Übungen überlegt. Die erste Aufgabe kannst du alleine lösen, die anderen gemeinsam mit deinem Partner. Es gibt zwei Pflicht-Aufgaben, die anderen sind optional. 

Wenn du mit den Übungen fertig bist, kannst du weiter mit dem Roboter spielen. Vergiss nicht den Fortschritt im Excel Sheet einzutragen.


```{exercise} Pflicht: Wie war die erste Woche in der IT?
:label: exercise-motivation-it-1

Beantworte dazu folgende Fragen über dich:
- Was hat dir diese Woche besonders gut gefallen? Wieso hat dir das besonders gut gefallen?
- Was hat dir diese Woche gar nicht gefallen? Wieso hat dir das nicht gefallen?
- Was glaubst du, sind deine drei besten Eigenschaften?

Beantworte die Aufgane in diesem [Dokument](https://docs.google.com/document/d/1oW26r6sBmvNLDb1kjRYjzjnu1JR5OdNVOqEvgYIly5s/edit#).
```

```{exercise} Pflicht: Was macht eine IT-Systemtechnikerin
:label: exercise-design-a-manual

In Österreich wurden vor kurzem die IT-Lehrberufe neu gestaltet und es sind drei Lehrberufe entstanden:
- Applikationsentwicklung Coding
- Informationstechnologie - Schwerpunkt Systemtechnik
- Informationstechnologie - Schwerpunkt Betriebstechnik

Du kannst dich zu allen Lehrberufen auf [it-lehre.wien](https://it-lehre.wien/) informieren.

Bei dieser Aufgabe schaust du dir zu einem Lehrberuf [in diesem Video](https://www.whatchado.com/de/stories/jasmin-gastgeb) an.

Beantworte folgende Fragen [hier](https://docs.google.com/document/d/1oW26r6sBmvNLDb1kjRYjzjnu1JR5OdNVOqEvgYIly5s/edit#):
- Was macht Jasmin Gastgeb?
- Was macht sie in ihrer Lehre?
- Findest du diese Lehre interessant?
```

```{exercise} Optional: Anforderungen und Missverständnisse
:label: exercise-requirements

Was möchte uns dieses Bild sagen?

![Requirements meme](https://miro.medium.com/max/996/1*nhVE3RkKyhk2kEh3nuk3Lw.png)

Fragen:
- Hat du im Alltag schon Situationen erlebt, wo du missverstanden wurdest oder du eine andere Person
falsch verstanden hast?
- Warum war das so?
- Was kann man tun, um solche Missverständnisse zu vermeiden?

Diskutiert im Team und schreibt eine Checkliste mit Dingne die wichtig sind, wenn
man Anforderungen formuliert.

Die Checkliste kann entweder auf einem Blatt Papier sein, das ihr der Betreuungsperson gebt
oder ihr fügt sie [hier](https://docs.google.com/document/d/1oW26r6sBmvNLDb1kjRYjzjnu1JR5OdNVOqEvgYIly5s/edit#) hinzu.
```


```{exercise} Optional: Schreibe eine Anleitung
:label: exercise-design-an-algorithm

In dieser Aufgabe werden alle Dinge, die du gelern hast zu einem vereint und ist schon fast ein kleines Projekt.
Projektbeschreibung:
- Überlege dir eine Aufgabe, die der Roboter erledigen soll (ähnlich wie wir was am Mittwoch für Olga gemacht haben). Wichtig: verwende nur Blöcke, die du und dein Partner kennen.
- Beschreibe die Aufgabe, die der Roboter machen soll.
- Schreibe einen Algorithmus auf Papier
- Tauschen mit deiner Partnerin die Algorithmen aus und probiert sie zu programmieren.

Sobald ihr beide fertig seid: Machen die Roboter was ihr wolltet?

Dokumentiert euer Erlebnis  [hier](https://docs.google.com/document/d/1oW26r6sBmvNLDb1kjRYjzjnu1JR5OdNVOqEvgYIly5s/edit#).
```
