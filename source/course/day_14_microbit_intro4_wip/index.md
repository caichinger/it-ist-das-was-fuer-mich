# Tag 14: BBC micro:bit Intro IV

{download}`🎬 Slides <./slides.pdf>`

Wir machen heute folgende Übungen:

- Am Mittwoch habt ihr euch ausführlicher zu Berufen und Ausbildungen
  unterhalten. Welche Berufe oder Ausbildungen könntest du dir für dich vorstellen?
  Trage deine Gedanken dazu [hier](https://docs.google.com/document/d/1tm0BvkZtS-oFaGNKqKZmDmYGNcyq4N16Fcz4SWbXYk8/edit?usp=sharing).
- {ref}`exercise-questions-for-sophie`
- {ref}`exercise-rock-paper-scissor-part-1`
