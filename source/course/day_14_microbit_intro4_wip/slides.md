---
marp: true
theme: default
header: '![width:200px](../../_static/ec_logo_cropped.png)'
footer: 'IT, ist das was für mich? Slides von Claus Aichinger (it-orientation@everyonecodes.io)'
paginate: true
---

# IT, ist das was für mich? 👩‍💻

## Tag 14

✂️ + 🪨 + 📑 = ❓

Gastvortragende Sophie Amelien - Spieleentwicklerin

---

# Intro 🌅

## Was passiert heute? 🗒️

- Überblick zu den letzten beiden Kurswochen
- Projekte (nach)besprechen
- Ein Spiel programmieren
- Gastvortragende Sophie Amelien,
  Spieleentwicklering bei [Mi’pu’mi Games](https://www.mipumi.com/)

---

## Allfälliges

- 🚪 Eingangstüre: bitte darauf achten, dass
  diese immer geschlossen ist (sie bleibt manchmal stecken)
- 😷 Maskenpflicht: bitte immer FFP2 Maske tragen
- 🤒 Krankmeldungen und Zeitbestätigungen: bitte ab jetzt
  (wenn möglich) per Mail

---

# Wie war Peer Learning? 🤔

- Was habt ihr gemacht?
- Wie ist es euch gegangen?

---

## Stand der Miniprojekte? 🏗️

- Wie weit seid ihr gekommen?
- Was braucht ihr noch?

---

# Wie geht es nach dem Kurs weiter?

- Wir werden uns dazu mit jeder von euch zusammen setzen.
- Wichtig: Bitte eure Wünsche in das Dokument eintragen.

---

## Was haben wir heute gelernt? 📝

## Begriffe & Zusammenhänge

*Was war euch wichtig?*

---

## Outro 🌆

### Was war heute?

- Berufsbilder und Perspektiven
- Spiel entwickelt
- Eine Spieleentwicklering kennen gelernt.

---

### Was kommt morgen

- Soft Skills mit Christina.
- Schwerpunkt Kommunikation und Präsentation.

---

## Retrospektive

- Was hast du gelernt?
- Was hat dich überrascht?
- Was nimmst du mit für morgen?

---

# Wir freuen uns auf morgen! 😃
