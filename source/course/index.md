# Kursmaterialien

Hier findet ihr einen Link zu jedem Termin mit einer kurzen
Beschreibung was wir an diesem Tag vor haben.

```{toctree}
:glob:
:maxdepth: 1

day_*/index
```
