# Tag 17: Web Intro II

**Wie war das mit Markdown, HTML und unserer Website?** 🌐 🤔

Wir haben zwei größere Themen die noch offen sind:
1. Wie aktualisieren wir unsere Kursseite mit euren Inhalten?
2. Wie setzen wir die einzelnen Teile unseres Schere-Stein-Papier-Spiels zu einem fertigen
   Programm zusammen?

Außerdem haben wir morgen zwei Gäste und deren Besuch wollen wir vorbereiten.

{download}`🎬 Slides <./slides.pdf>`

Wir machen heute folgende Übung(en):

- {ref}`exercise-questions-for-sophie`
- {ref}`exercise-questions-for-dagmar`
- {ref}`Update der Kursseite <website_update_target>`
- {ref}`Schere-Stein-Papier zusammen führen <exercise-rock-paper-scissor-part-2>`


```{exercise} Unsere Fragen an Dagmar (Projektleiterin & Scrum Master)
:label: exercise-questions-for-dagmar

Dagmar Labes ist Projektleiterin und Scrum Master.
Sie arbeitet bei [Sportradar](https://www.sportradar.com/).

Sie wird uns morgen besuchen.

Fragen:
* Was macht die Firma?
* Welche Fragen möchtest du Dagmar stellen?

Fragen an Dagmar:
* Was passiert in Wien und was an anderen Orten?
* Wie viele Abteilungen gibt es bei Sportradar in Wien?
* Wo kommen die Daten (bspw. von Spielen) her?
* Wie hast du mit IT begonnen?
* In welchem Bereich von Sportradar bist und was  
  was machst du da?
* Hast du Tipps für mich, wenn ich in der IT Fuß 
  fassen möchte?
* Was ist schwierig im Job?
* Welche Eigenschaften musst du als Projektleiterin
  mitbringen?
* Gefällt dir deine Tätigkeit als Projektleiterin
  besser als das, was du davor gemacht hast?
* Wie arbeitet ihr (im Team) zusammen?
* Wie sieht dein Berufsalltag aus?
```


```{exercise} Unsere Fragen an Sophie (Game Dev)
:label: exercise-questions-for-sophie

Sophie Amelien ist Spieleentwicklerin.
Sie arbeitet als Programmiererin bei [Mi’pu’mi Games](https://www.mipumi.com/).

Sie wird uns morgen besuchen.

Fragen:
* Was macht die Firma?
* Welche Fragen möchtest du Sophie stellen?

Fragen an Sophie:
* Wolltest du schon immer als Programmiererin arbeiten?
* Wie bist du zu Mipumi gekommen?
* Was machst du bei Mipumi?
* War es für dich eine gute Entscheidung in dieser Branche (als Frau) tätig zu sei?
* Wie sieht dein Berufsalltag aus?
* Wie sieht deine/eure Zusammenarbeit aus?
* Was ist schwierig in deinem Job?
* Welche Eigenschaften sollte man deiner Meinung nach mitbringen?
* Möchtest auch in Zukunft als Game Dev arbeiten?
```
