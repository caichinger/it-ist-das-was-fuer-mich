---
marp: true
theme: default
header: '![width:200px](../../_static/ec_logo_cropped.png)'
footer: 'IT, ist das was für mich? Slides von Claus Aichinger & Olga Drewitz (it-orientation@everyonecodes.io)'
paginate: true
---

# IT, ist das was für mich? 👩‍💻

## Tag 2 🎈

* Grundbegriffe
* Laptop in Betrieb nehmen
* Computer zerlegen

---

## Intro 🌅

*Big Picture*

---

## Was passiert heute? 🗒️

- Abschluss Administration
- Grundbegriffe
- Laptops in Betrieb nehmen
  - Software installieren
  - Wichtige Webseiten einrichten
- Computer zerlegen
  - Komponenten?
  - Eigenschaften?
  - Funktionen?

---

## Abschluss Administration

- Braucht noch jemand etwas?
- DLU?
- Datenschutzvereinbarung ausfüllen

---

## Grundbegriffe 📖

- Was kennen wir schon?
- Was brauchen wir heute?

---

## Laptops in Betrieb nehmen 💻

Zu installieren:
- Google Chrome Browser
- Lego Software

Optional:
- Bildschirmhelligkeit
- Datum und Zeit richtig einstellen

---

## Pause ☕

*Gleich geht's weiter.*

---

## Computer ❓ 💻 👀

- Woraus besteht ein Computer?
- Was haben Computer gemeinsam?

---

### Woraus besteht ein Computer

Wir sprechen über Hardware
- Komponenten benennen und einordnen können
- Eigenschaften beschreiben

Fragen:
- Was braucht ein Computer (damit er funktioniert)?
- Was haben verschiedene Computer gemeinsam?
- Welche Aufgabe erfüllt welche Komponente?

---

### Woraus besteht ein Computer?

Was wisst ihr schon?

---

### Übung: Was machen folgende Komponenten?

Komponenten
- Prozessor
- Arbeitsspeicher
- Festplatte

Beantwortet in Gruppen folgende Fragen:
- Welche Rolle spielen die Komponenten?
- Mit welchen Kennzahlen werden sie beschrieben?
- Wie sieht das bei eurem Smartphone aus?

---

## Pause ☕

*Gleich geht's weiter.*

---

## Was haben wir heute gelernt? 📝

## Begriffe & Zusammenhänge

*Was war euch wichtig?*

---

## Outro 🌆

*Tagesabschluss*

---

### Was war heute?

- Viele wichtige Begriffe
- Laptops und Webseite kennen gelernt
- Computer von innen gesehen

---

### Was kommt morgen?

- Aufgaben beschreiben
- Kann Olga den Lego Roboter programmieren?

---

## Retrospektive

- Was hast du gelernt?
- Was hat dich überrascht?
- Was nimmst du mit für morgen?

---

# Wir freuen uns auf morgen! 😃
