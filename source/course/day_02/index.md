# Tag 2: Grundbegriffe, Laptops, Computer

**Wir zerlegen einen Computer!** 🧰 🪛 💻

Zu tun:
- Bitte [Datenschutzvereinbarung](https://docs.google.com/forms/d/e/1FAIpQLScPMdDD_as01FAxDbmCKG8wuAOAdGorKJVdl0uflEUDHfmzOA/viewform?usp=sf_link) ausfüllen.
- Bitte [Lego Software](https://education.lego.com/de-de/downloads/retiredproducts/mindstorms-ev3-lab/software) herunterladen und installieren.

Heute werden wir die Laptops in Betrieb nehmen und alle notwendigen Programme installieren.
Nebenbei sammeln wir nützliche Begriffe um zu beschreiben, was wir tun.
Außerdem werden wir einen Computer zerlegen und uns anschauen, woraus der besteht.

{download}`🎬 Slides <./slides.pdf>`
