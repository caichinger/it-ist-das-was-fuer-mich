# Tag 1: Intro

**Willkommen im Kurs!** 😃

Der heutige Tag steht ganz im Zeichen des gegenseitigen Kennenlernens.
Außerdem werden wir uns überlegen, wie wir zusammenarbeiten wollen und
besprechen, wie die kommenden Wochen aussehen.

{download}`Slides <./Tag01_Intro_Kennenlernen_Admin.pdf>`
