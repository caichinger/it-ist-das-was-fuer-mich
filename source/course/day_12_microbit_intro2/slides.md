---
marp: true
theme: default
header: '![width:200px](../../_static/ec_logo_cropped.png)'
footer: 'IT, ist das was für mich? Slides von Claus Aichinger (it-orientation@everyonecodes.io)'
paginate: true
---

# IT, ist das was für mich? 👩‍💻

## Tag 12

> Wie entwickelt man ein Programm?

---

# Administratives

- 🧪 Bitte Testnachweise
- 📅 Bitte regelmäßige PCR-Tests (Dienstag und Donnerstag)
- Frage: Wer könnte grundsätzlich von zu Hause mitmachen?


---

# Intro 🌅

## Was passiert heute? 🗒️

- Kleine Wiederholung
- Wie entwickelt man ein Programm?

---

# Wiederholung

- Wie setzt man einen Rechner auf?
- Was erstellt und führt man ein Programm auf dem Microbit aus?

---

# Wie entwickelt man ein Programm? I

1. Analysiere Aufgabe:
  - Welche Dinge gibt es?
  - Wie hängen diese Dinge zusammen?
  - Wie verhalten sie sich?
2. Beschreibe die Aufgabe:
  - Schreibe ein paar Stichworte zusammen
  - Erstelle eine Skizze

---

# Wie entwickelt man ein Programm? I

Dann... im Fall von Make Code:
- Welche Blöcke brauche ich?

3. Übersetze die Beschreibung/Skizze in Code
  - Blöcke erstellen
  - Blöcke verknüpfen

---

# Wie entwickelt man ein Programm? II

- Kann ich die Aufgabe vereinfachen?
- Kleine Schritte
- Ausprobieren (nach jedem Schritt)

---

# Weiter auf unserer Website

**https://caichinger.gitlab.io/it-ist-das-was-fuer-mich/**

---

# Outro 🌆

## Was haben wir heute gemacht? 📝

- Wichtige Begriffe
- Wichtige Zusammenhänge

---

## Was kommt morgen?

- Weiter beim Microbit

---

## Retrospektive

- Was hast du gelernt?
- Was hat dich überrascht?
- Was nimmst du mit für morgen?

---

# Ich freue mich auf morgen! 😃
