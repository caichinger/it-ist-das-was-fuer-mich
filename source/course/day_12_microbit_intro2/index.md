# Tag 12: Programmieren mit dem Microbit I

**Wie entwickelt man ein Programm?** ⚙️ 💻 🤔

Gestern haben wir den Microbit in Betrieb genommen und wissen nun,
wie man ein einfaches Programm erstellt.
Heute wollen wir der Frage nachgehen, wie man etwas komplexere Aufgaben
angehen und lösen kann.

{download}`🎬 Slides <./slides.pdf>`


Wir machen folgende Übungen:
1. {ref}`exercise-micro-bit-dice`
1. {ref}`exercise-micro-bit-emoji-dice`
1. {ref}`exercise-micro-bit-retrospective` (optional)
