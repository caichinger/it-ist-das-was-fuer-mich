# Documentation

## Gedanken

* Mindset
* Wiederholung
  * Begriffe
  * Wie erstelle ich ein Programm?
  * Wie kopiere ich ein Programm?
  * Was für Blöcke braucht mein Programm jedenfalls?
* Vertiefung und Diskussion
  * HEX File, warum kann ich das unter Windows nicht öffnen
  * Was ist dieses HEX File überhaupt?
  * Kopieren vs Öffnen einer Datei, Keyboard Shortcuts, Window Management
* Weiter beim Programmieren
  * Abwandlung vom Würfel
  * Emoji Barometer gemeinsam
  * Wearable?

Stärkerer Fokus auf Fragen "Was? Warum? Wie?".

## Ablauf

Siehe Slides.
Fachlicher Inhalt siehe Website.

## Retro & Notizen

