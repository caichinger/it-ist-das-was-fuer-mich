---
substitutions:
  feedback_link: '[Feedback](https://docs.google.com/forms/d/e/1FAIpQLSdoGAZXZ-yFCyKIfl4GOzTW0Z_r2sgSNF8ywspQp07QCfa1EA/viewform?usp=sf_link)'
---
(tag20_project_kickoff_target)=
# Tag 20: Projektstart und Peer Learning

**Projekt Kick Off!** 🚀

{download}`🎬 Slides <./slides.pdf>`

Die Zeit ist gekommen, dass ihr eure ganz eigenen Ideen umsetzt!

Manche von euch haben sich dazu schon Gedanken gemacht, andere sind noch
am Überlegen. So oder so, steht heute die Planung und Beschreibung
eures Projekts im Vordergrund:
* Worum geht es?
* Was benötigt ihr zur Umsetzung?
* Wie werdet ihr es angehen?
* Werdet ihr Olga's oder Claus' Unterstützung brauchen?

Fragen wie diese wollen wir im Rahmen der Projektbeschreibung beantworten.


## Feedback

% supply link to feedback form through the feedback_link variable at the top
{{ ask_for_feedback }}
