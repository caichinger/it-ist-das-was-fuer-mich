# Tag 11: Raspberry PI und BBC micro:bit Intro

**Wie war das nochmal mit Computern und Programmen?** 💻🤔

Wir werden uns heute zwei weitere Computer anschauen und in Betrieb nehmen:
- [Raspberry PI](https://www.raspberrypi.com/)
- [BBC micro:bit](https://microbit.org/)

Obwohl so klein, ist der Raspberry PI schon ein ausgewachsener Computer.
Der Microbit ist mehr zum Basteln gedacht und wir werden ihn
als Plattform für unsere Programmierprojekte verwenden.

{download}`🎬 Slides <./slides.pdf>`

Wir machen heute folgende Übungen
1. {ref}`exercise-raspberry-pi-description`
1. {ref}`exercise-raspberry-pi-setup-1`
1. {ref}`exercise-micro-bit-chrome-webusb`
1. {ref}`exercise-micro-bit-name-tag`
1. {ref}`exercise-micro-bit-dice`
