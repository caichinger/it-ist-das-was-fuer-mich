# Razia Nazari
2000 | [Afghanistan](https://de.wikipedia.org/wiki/Afghanistan)

## Über mich
* ♒
* 💻 Mein Leidenschaft
* 🔜 Matura

## Was wollst du machen?
*  Ich möchte software Developer studieren.


## Welche Ausbildungen klingen für dich interessant?

* Softwareentwickler (Der Softwareentwickler programmiert Softwareanwendungen nach den Vorstellungen der Kunden und testet diese.)
* Applikationsentwicklung- Coding (Programmieren/Codieren von Applikationen oder Applikationsteilen.)


## Projekt

Präsentation: [Google Slides](https://docs.google.com/presentation/d/1xBMOfSWisLkl5ZNlszTt9UhY5ruAoyhrdn-Ir-w96Ys/edit?usp=sharing)

Projektbeschreibung: [Dokument](https://docs.google.com/document/d/1eJKJiqoz2dC8bDkYU6MHa99-CaZbG5nXQLzMYEdvTK0/edit?usp=sharing) (falls die lieber im Google Doc als auf der Webseite arbeitest)
