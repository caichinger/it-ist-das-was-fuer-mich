# Michelle Chvojka

## Welche Ausbildungen klingen für dich interessant?

*Füge hier eine Liste und Beschreibungen ein.*

---

## Projekt

Präsentation: [Google Slides](https://docs.google.com/presentation/d/1fwo1O9CejeEOTsThFJhBkBfsQ6KAFXvWEUo49PMbbkQ/edit?usp=sharing)

Projektbeschreibung: [Dokument](https://docs.google.com/document/d/1OH6V8EmN4YZq1mUzg_yyzNgXpnafP5aydaRPFSS7Dug/edit?usp=sharing) (falls die lieber im Google Doc als auf der Webseite arbeitest)
