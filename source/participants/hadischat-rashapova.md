# Hadischat Rashapova

1998|Tschetschenisch

## Meine Interessen & Hobbys
* Lesen (russische, englische & französische Literatur)
* Texte verfassen (z.B. Buchzusammenfassungen)
* Auto fahren

## Ausbildung und Berufserfahrung
* Akademisches Gymnasium (Wien)
* Abendgymnasium (Linz)
* Volkshochschule (Wien)
* Teilzeitjobs (Einzelhandel, Call Center)

## Was hat dir im It-Kurs gefallen?
Besonders das Arbeiten mit dem Roboter, das Zerlegen des
Computer und vor allem das Programmieren der Microbits. Die
Kursleiter sind wirklich sehr freundlich und professionell und sie haben uns eine perfekte Mischung aus theoretischem Input und Ausprobieren geboten. Alles in allem war ich sehr zufrieden mit dem Kurs. Es war anspruchsvoll, interaktiv, man hat direkt Hilfe bekommen wenn etwas mal nicht geklappt hat. Außerdem hat es sehr viel Spaß gemacht das Gelernte anschließend anzuwenden. Man hat uns auch nur die wirklich notwendige Theorie gelehrt. Die Gruppenatmoshphäre war sehr angenehm und es hat viel Spaß gemacht im Team zuarbeiten.
Mir hat der Kurs allgemein sehr gut gefallen. Besonders das Arbeiten mit dem Roboter, das Zerlegen des Computer und vor allem das Programmieren der Microbits. Die Kursleiter sind wirklich sehr freundlich und professionell, sie haben uns eine perfekte Mischung aus theoretischem Input und Ausprobieren geboten.


## Was willst du machen?
Ich weiß noch nicht genau was ich machen möchte, aber auf jeden Fall etwas im Bereich Medizin oder IT.

## Welche Ausbildungen klingen für dich interessant?

* Software Developer
* Web Developer


## Projekt

Präsentation: [Google Slides](https://docs.google.com/presentation/d/1ZJ289IswSw7O7M78O15mL1fYPiZQdJ7CrO5OjlUP6Js/edit?usp=sharing)

Projektbeschreibung: [Dokument](https://docs.google.com/document/d/1OpG9v9Nny5P9DdnXNQURTbj8yHVEgambe1ievs-QMGE/edit?usp=sharing) (falls die lieber im Google Doc als auf der Webseite arbeitest)
