# Lesina Haicharoeva

## Welche Ausbildungen klingen für dich interessant?

*Füge hier eine Liste und Beschreibungen ein.*

## Projekt

Präsentation: [Google Slides](https://docs.google.com/presentation/d/1WYBsi6qanh3RVrCvziNNNrOzlmUiJQyyvOMg7i7fGIE/edit?usp=sharing)

Projektbeschreibung: [Dokument](https://docs.google.com/document/d/190qj5qNfEKRu4E0FG1gdEpjS27byT5LNqEDvCwVN6Lw/edit?usp=sharing) (falls die lieber im Google Doc als auf der Webseite arbeitest)
