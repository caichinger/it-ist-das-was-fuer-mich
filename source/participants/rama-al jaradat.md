# Rama Al Jaradat

* 1997/ Syrien

## Ausbildung und Berufserfarung:

* Hauptschule, Mittelschule, Gymnasium mit Maturaabschluss (Syrien).
* Vorstudienlehrgang, VWU (Wien).
* Praktikum als Ordinationsassistentin und Kindergartenassistentin (Wien).

## Meine Interessen:

* Lesen (Romane, Bücher).
* Fotografieren.
* Sport.

## Sprachen:

* Arabisch.
* Englisch.
* Deutsch.

## Was willst du machen ?

* Ich weiß noch nicht was ich machen möchte, aber auf jeden Fall eine Ausbildung, die man in einer kurzen Zeit absolvieren kann.



## Welche Ausbildungen klingen für dich interessant?


## Projekt

Präsentation: [Google Slides](https://docs.google.com/presentation/d/1x0ovZjApyka8gZjER9sHzr6Buz1CZYFi3GLd1ba9dJM/edit?usp=sharing)

Projektbeschreibung: [Dokument](https://docs.google.com/document/d/1JoEYXcpxuOu7ktfzEbk6zz8jOyRHr09Bi6DwXNmZapY/edit?usp=sharing) (falls die lieber im Google Doc als auf der Webseite arbeitest)
