# Berufsorientierung

```{important}

Wie geht es weiter? Auf dieser Seite findest du einen Überblick. Fange ruhig schon an, {{ '[Ideen zu sammeln und dir Notizen]({})'.format(doc_Berufe_und_Kurse) }} zu machen! 😃

```


## Frauen in Technik - Angebote über das AMS

>  Sie möchten einen Job mit Perspektive, in dem Sie gut verdienen? Haben Sie schon an eine
Ausbildung im Bereich „Handwerk und Technik“ gedacht? Denn gut ausgebildete Fachkräfte sind
am Arbeitsmarkt sehr gefragt – und gut bezahlt.

Über das [FiT-Programm](https://www.ams.at/arbeitsuchende/aus-und-weiterbildung/fit-frauen-in-handwerk-und-technik) des AMS werden **mehr als 200 verschiedene Ausbildungen in Handwerk und Technik gefördert**.

* Welche Ausbildungen sind das? [Liste aller Fit Ausbildungen](https://www.ams.at/content/dam/download/allgemeine-informationen/001_fit_ausbildungsliste.pdf)
* Was noch? Zusätzlich werden noch weitere Ausbildungen für **Digitale Jobs & Green Jobs** gefördert.

Wenn du dir bei manchen Berufen oder Ausbildungen unsicher bist, worum es geht,
dann helfen dir vielleicht der Karrierekompass und der Ausbildungskompass weiter - diese
bieten Beschreibungen.


## Berufe und Ausbildungen

Es gibt eine Vielzahl Berufen und Ausbildungen:

* Welche Berufe gibt es? [Karrierekompass](https://www.karrierekompass.at/)
* Welche Ausbildungen gibt es? [Ausbildungskompass](https://www.ausbildungskompass.at/)
* Welche Lehren gibt es und wo werden sie angeboten? [Lehrbetriebsübersicht der WKO](https://lehrbetriebsuebersicht.wko.at/)


*Der Beruf ist das Ziel, die Ausbildung ist der Weg dorthin.*


## Tech She Likes - Vorbilder, Job Profile, ...

> “techshelikes” ist eine deutschsprachige Plattform mit dem Ziel die Technologie-Branche für
Mädchen und Frauen zugänglicher und interessanter zu machen und die Vielfalt an Jobs mit
“purpose & impact” in dieser Branche aufzuzeigen. Dabei arbeiten wir bewusst mit Vorbildern
bzw. interviewen im Rahmen unseres Podcasts interessante Frauen und lassen sie über ihre
Geschichten und Erfahrungen als Mädchen sowie Frau in der Tech-Branche und in der Gesellschaft
erzählen.

Auf [tech she likes](https://techshelikes.co/) findest du eine bunte Übersicht zu
* Vorbilder
* Job Profile
* Wege in Tech

## Let's Tech - Alles über Elektrotechnik & IT

> Etwas Neues aus­probieren. Etwas Neues schaffen. Alte Klischees zurück­lassen. Die Welt mit offenen Augen sehen. Von welcher Welt wir sprechen? Von der Welt der Elektrotechnik
und IT. Der Welt, die wir euch näher­bringen wollen. Ganz nahe. Als Initia­tive des OVE
– des öster­rei­chischen Verbandes für Elektro­technik. Aber speziell für Schülerinnen und Schüler. Speziell für dich .

Auf [Let's Tech](https://www.letstech.at/) findest du
* [Einblicke in Berufsbilder](https://www.letstech.at/lit-up/?_tag=tech-jobs#beitraege),
* kannst dir verschiedene [Jobs](https://www.letstech.at/jobs-und-co/) anschauen

und vieles mehr.

## Betreuungsangebote für Mädchen und Frauen

### [Mädchen-Berufs-Zentrum (MBZ) bei sprungbrett!](https://sprungbrett.or.at/)

> Ein besonderes Angebot des AMS Wien für Mädchen und junge Frauen,
individuelle und punktgenaue Unterstützung bei der Planung ihrer
Berufsausbildung bzw. Berufswahl nützen wollen; hier erhalten sie
kompetente Begleitung bei der Berufsorientierung und allem,
das (sie) bewegt!

### [Frauenberufszentrum (FBZ) Wien](https://www.abzaustria.at/angebote-projekte/abz-frauenberufszentrum-wien)

> Das neue FBZ Wien bietet Kundinnen des AMS Wien besondere
Beratungs- und Betreuungsangebote. Das FBZ unterstützt Frauen
individuell und kostenlos. Neben Einzelberatungen gibt es
Workshop-Angebote zu unterschiedlichen Themenschwerpunkten,
aus denen Kundinnen auswählen können.

### [Bildungswege mit Lobby16](https://www.lobby16.org/projekt-bildungswege/)

> Mit unserem Projekt qualifizieren wir seit 2010 jährlich rund 40 Jugendliche mit Fluchterfahrung und vermitteln sie
in eine Lehrausbildung.
> Gemeinsam mit Unternehmen und unserem Netzwerk an ehemaligen Projektteilnehmer*innen und Ehrenamtlichen bilden wir eine Brücke zwischen dem Pflichtschulabschluss und dem Einstieg in den Arbeitsmarkt.
> Wir wissen, dass der Beginn und das Durchhalten herausfordend sein können - wir begleiten die Jugendlichen daher in ihre Lehre und sind bis zum Lehrabschluss für sie da!


### [A|B|O Jugend](https://abo-jugend.at/)

> Du bist zwischen 15 und 21 Jahre alt, beim AMS Wien vorgemerkt und suchst eine Lehrstelle oder einen Job. Dann komm zu A|B|O Jugend!
> Wir unterstützen dich bestmöglich beim Berufseinstieg!
> Die Zubuchung zu A|B|O Jugend erfolgt über das AMS Wien Jugendliche.


## Weitere Aus- und Weiterbildungsangebote

* WAFF
* BFI
* https://codersbay.wien/
* ...
